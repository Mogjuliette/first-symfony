<?php

namespace App\Controller;

use App\Repository\PersonRepository;
use App\Entities\Person;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

#[Route('/api/person')]
class PersonController extends AbstractController
{
    /**
     * @param PersonRepository $repo
     */
    public function __construct(private PersonRepository $repo) {}

    #[Route(methods:'GET')]
    public function persons(Request $request):JsonResponse{
        $page = $request->query->get('page', 1);
        $pageSize = $request->query->get('pageSize', 15);
        return $this->json(
            $this->repo->findAll($page, $pageSize)
        );

    }

    #[Route('/{id}', methods:'GET')]
    public function personId(int $id):JsonResponse{
        $person = $this->repo->findById($id);
        if(!$person){
            throw new NotFoundHttpException();
        } 
        return $this->json($person);
    }

    #[Route(methods:'POST')]
    public function add(Request $request, SerializerInterface $serializer):JsonResponse{
        $person = $serializer->deserialize($request->getContent(), Person::class, 'json');

        $this->repo->persist($person);
        return $this->json($person, Response::HTTP_CREATED);
    }

    #[Route(methods:'DELETE')]

    public function deleteById(Request $request):JsonResponse{
        $id = $request->query->get('id');
        $count = $this->repo->delete($id);
        if($count > 0){   
            return $this->json($count, 204);
        } else {
            throw new NotFoundHttpException;
        }
    }
    
    #[Route(methods:'PUT|POST')]

    public function update(Request $request,  SerializerInterface $serializer):JsonResponse{
        $person = $serializer->deserialize($request->getContent(), Person::class, 'json');
        $this->repo->update($person);
        return $this->json($person, Response::HTTP_OK);
    }
}