<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FirstController extends AbstractController {

    #[Route('/')]
    public function homepage(){
        return $this->json('coucou');
    }

    #[Route('/greet/{slug}')]
    public function greet(String $slug){
        return $this->json("Hello {$slug}");
    }
    
}
;