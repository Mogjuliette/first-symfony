<?php

namespace App\Controller;
use App\Repository\DogRepository;
use App\Entities\Dog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/dog')]
class DogController extends AbstractController{
    private DogRepository $repo;

    /**
     * @param DogRepository $dogs
     */
    public function __construct(DogRepository $repo) {
    	$this->repo = $repo;
    }

    #[Route(methods:'GET')]
    public function dog(){
        $myDogs =  $this->repo->findAll();
        return $this->json($myDogs);
    }

    #[Route('/{id}', methods:'GET')]
    public function dogId(int $id){
        $myDog = $this->repo->findById($id);
        if(!$myDog){
            throw new NotFoundHttpException();
        } 
        return $this->json($myDog);
    }

    #[Route(methods:'POST')]
    public function add(Request $request, SerializerInterface $serializer){
        $dog = $serializer->deserialize($request->getContent(), Dog::class, 'json');

        $this->repo->persist($dog);
        return $this->json($dog, Response::HTTP_CREATED);
    }
}