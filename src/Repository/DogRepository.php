<?php

namespace App\Repository;
use App\Entities\Dog;

use PDO;
use DateTime;
class DogRepository{
        
    private PDO $connection;
    public function __construct() {
        $this->connection = new PDO('mysql:host=localhost;dbname=p23_first', 'simplon', '1234');
    }

    /**
     * Summary of findAll Faire une requête SQL vers la base de données et convertir les résultats de cette requête en instances de la classe Dog
     * @return Dog[]
     */
    public function findAll(): array{
    
        $dogArray = [];

        $statement = $this->connection->prepare('SELECT * FROM dog');
        
        $statement->execute();
        
        $results = $statement->fetchAll();
        
        foreach($results as $item){
            $dogArray[] = new Dog($item['name'], $item['breed'], (new DateTime($item['birthdate'])), $item['id']);
        }
        return $dogArray;
    } 

    public function persist(Dog $dog){

        $statement = $this->connection->prepare('INSERT INTO dog (name, breed, birthdate) VALUES ( :name , :breed , :birthdate )');

        $statement->bindValue('name',$dog->getName());
        $statement->bindValue('breed', $dog->getBreed());
        $statement->bindValue('birthdate', $dog->getBirthdate()->format("Y-m-d"));
        
        $statement->execute();
        
    }

    public function findById(int $id):?Dog{

        $statement = $this->connection->prepare('SELECT * FROM dog WHERE id = :myId ');
        $statement->bindValue('myId', $id);
        
        $statement->execute();
        
        $result = $statement->fetch();

        if($result){
            return $this->sqlToDog($result); 
        } else {
            return null;
        } 
    }

    private function sqlToDog(array $line):Dog{
        $birthdate = null;
            if(isset($item['birthdate'])){
                $birthdate = new DateTime($line['birthdate']);
            }
            return new Dog($line['name'], $line['breed'], $birthdate, $line['id']);
    }
}