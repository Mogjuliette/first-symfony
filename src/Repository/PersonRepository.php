<?php

namespace App\Repository;

use App\Entities\Dog;
use App\Entities\Person;
use PDO;


class PersonRepository{

    private PDO $connection;
    public function __construct() {
        $this->connection = new PDO('mysql:host=localhost;dbname=p23_first', 'simplon', '1234');
    }

    /**
     * Summary of findAll Faire une requête SQL vers la base de données et convertir les résultats de cette requête en instances de la classe Person
     * @return Person[]
     */
    public function findAll(int $pgNb = 1, int $nb = 15): array{
    
        $personArray = [];
        if($nb <= 0){
            $first = 0;
        } else {
            $first = ($pgNb * $nb) - $nb;
        }
        $statement = $this->connection->prepare('SELECT * FROM person LIMIT :usedNb OFFSET :first');
        $statement->bindValue('first', $first, PDO::PARAM_INT);
        $statement->bindValue('usedNb', $nb, PDO::PARAM_INT); 
        $statement->execute();
        
        $results = $statement->fetchAll();
        
        foreach($results as $item){
            $personArray[] = $this->sqlToPerson($item);
        }
        return $personArray;
    } 

    public function findById(int $id):?Person{

        $statement = $this->connection->prepare('SELECT * FROM person WHERE id = :myId ');
        $statement->bindValue('myId', $id);
        
        $statement->execute();
        
        $result = $statement->fetch();

        if($result){
            return $this->sqlToPerson($result); 
        } else {
            return null;
        } 
    }

    private function sqlToPerson(array $line):Person{
        return new Person($line['firstname'], $line['name'], $line['id']);
    }

    public function persist(Person $person){

        $statement = $this->connection->prepare('INSERT INTO person (firstname, name) VALUES ( :firstname , :name)');

        $statement->bindValue('firstname',$person->getFirstname());
        $statement->bindValue('name', $person->getName());
        
        $statement->execute();
        $person->setId($this->connection->lastInsertId());
        
    }

    public function delete(int $id):int{
        $this->connection->exec("SET foreign_key_checks = 0");
        $statement = $this->connection->prepare('DELETE FROM person WHERE id = :id');
        $statement->bindValue('id',$id, PDO::PARAM_INT);
        $statement->execute();
        $count = $statement->rowCount();
        $this->connection->exec("SET foreign_key_checks = 1");
        return $count;
    }

    public function update(Person $person){
        $statement = $this->connection->prepare('UPDATE person SET firstname = :firstname, name = :name WHERE id = :id');
        $statement->bindValue('id', $person->getId(), PDO::PARAM_INT);
        $statement->bindValue('firstname', $person->getFirstname(), PDO::PARAM_STR);
        $statement->bindValue('name', $person->getName(), PDO::PARAM_STR);
        $statement->execute();
    }

    public function personDog (int $idDog){
        $statement = $this->connection->prepare('SELECT person.firstname, dog.name dname FROM person_dog JOIN `person` ON person_dog.id_person = person.id JOIN `dog` ON person_dog.id_dog = dog.id WHERE person_dog.id_dog = :idDog');
        $statement->bindValue('id', $idDog, PDO::PARAM_INT);
        $statement->execute();
        $results = $statement->fetchAll();
        $line = [];
        foreach($results as $result){
            $line[] = ($result["dname"]);
        }
        return $line;
    }
}