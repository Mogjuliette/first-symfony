-- Active: 1674203942125@@127.0.0.1@3306@p23_first

SELECT person.firstname, dog.name FROM person_dog 
JOIN `person` ON person_dog.id_person = person.id 
JOIN `dog` ON person_dog.id_dog = dog.id WHERE person_dog.id_dog = 3;

DROP TABLE IF EXISTS person_dog;

DROP TABLE IF EXISTS person;

DROP TABLE IF EXISTS dog;

DROP TABLE IF EXISTS address;

CREATE TABLE person (
    id INT AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(100),
    name VARCHAR(100)
);

INSERT INTO person (firstname, name) VALUES ("Juliette","Suc"),("Ariane","Suc"),("Marika","Huguier-Suc"),("Fae","Mayot"),("Adler","Araceli"),("Alizée","Gomès"),("Marcus","Faravel"),("Polaris","Faravel"),("Gali","Wright"),("Yué","Kaito"),("Lyle","Kaito"),;

CREATE TABLE dog (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    breed VARCHAR(100),
    birthdate DATE
);

INSERT INTO dog (name, breed, birthdate) VALUES ("Fido","Corgi","2021-05-03"),("Vanille","Labrador","2021-05-03"),("Fido","Samoyede","2021-05-03"),("Mayuko","Samoyede","2021-05-03"),("Yona","Husky","2021-05-03"),("Medor","Dalmatien","2021-05-03"),("Loki","Fox-Terrier","2021-05-03"),("Thor","Braque Hongrois","2021-05-03"),("Saly","Labrador","2021-05-03"),("Ruby","Chihuahua","2021-05-03"),("Mara","Berger Allemand","2021-05-03"),("Hyacinthe","Fox-Terrier","2021-05-03");

CREATE TABLE person_dog (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_person int,
    id_dog int,
    Foreign Key (id_person) REFERENCES person(id),
    Foreign Key (id_dog) REFERENCES dog(id)
);

INSERT INTO person_dog (id_person, id_dog) VALUES (1, 2),(2, 4),(3, 6),(3, 5),(1, 7),(4, 2),(4, 8),(2, 3);  

CREATE TABLE adress (
    id INT AUTO_INCREMENT PRIMARY KEY,
    number VARCHAR(8),
    street VARCHAR(100),
    city VARCHAR(100),
    zip_code VARCHAR(8),
    id_person int,
    Foreign Key (id_person) REFERENCES person(id)
);

INSERT INTO address(number, street, city, zip_code, id_person) VALUES ("82", "rue Jean Sarrazin", "Lyon", "69008", 1),("1 bis", "rue de la Loge", "Lyon", "69005", 2),("5", "rue Louis Malle", "Villeurbanne", "69100", 3);